<?php
namespace THelper;

/**
 * 
 */
class THelper
{
	
	/**
     * @desc    xmsb_curl        发送curl请求
     * @param string
     * $url 请求地址
     * @param array $data post请求的参数
     * @param array $header 头部信息
     * @return  string
     */
    function XmsbCurl($url, $data = [], $header = [])
    {
        // 初始化curl
        $curl = curl_init();

        // 绑定url
        curl_setopt($curl, CURLOPT_URL, $url);

        // 绑定头部信息
        if (empty($header)&&!empty($data)) {
           $header = [
                'Content-Type: application/json; charset=utf-8',
                'Content-Length:' . strlen($data),
                'Accept: application/json',
            ];
        }
        if(!empty($header)){
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        }
        
        // 为1时返回结果中将带有响应头信息
        curl_setopt($curl, CURLOPT_HEADER, 0);

        // 验证服务器证书有效性，0为跳过，2为开启
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        // 检验证书中的主机名和你访问的主机名是否一致，1为开启
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        // 判断请求类型是否为post
        if (!empty($data)) {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }

        // 为0时直接输出返回结果
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        // 发送请求
        $result = curl_exec($curl);
        curl_close($curl);
        $result = json_decode($result, true);
        return $result;
    }
}